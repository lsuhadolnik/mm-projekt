function [U,S,V] = jacobiAlg(A)

om = 1e-6;
pg = 100;

n = length(A);

U = A;
V = eye(n);


while pg > om
pg = 0;


for j = 2:n
  for i = 1:j-1
  
  % For each pair (i,j) where i < j

	% Seštej
  al = sum(sum(U(1:n,i).^2));
  be = sum(sum(U(1:n,j).^2));
  ga = sum(sum(U(1:n,i).*U(1:n,j)));

	pg = max(pg, abs(ga)/sqrt(al*be));

  th = (be - al)/(2*ga);
  t = sign(th)/(abs(th) + sqrt(1+th^2));

  c = 1/sqrt(1+t^2);
  s = c*t;

	t=U(:,i);
  U(:,i)=c*t-s*U(:,j);
  U(:,j)=s*t+c*U(:,j);
  

  t = V(:,i);
  V(:,i) = c*t - s*V(:,j);
  V(:,j) = s*t + c*V(:,j);


  end;
end;

end

% the singular values are the norms of the columns of U
% the left singular vectors are the normalized columns of U
for j=1:n
  S(j)=norm(U(:,j));
  U(:,j)=U(:,j)/S(j);
end
S=diag(S);

end
