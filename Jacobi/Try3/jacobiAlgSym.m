function [U,S,V, numIter] = jacobiAlgSym(A)

OA = A;

% Razširim matriko A
za = zeros(size(A));
A = [za A'; A za];

KA = A;

% N je dolžina stranice
N = length(A);
R = eye(N); % Tu bodo lastni vektorji

offDiagElems = 1;
numIter = 0;

while offDiagElems > 1e-10

  [p,q] = findAbsMaxTriu(A);

  % Privzeto... Če je element na A(p,q) enak 0
  c = 1; s = 0;

  % Izračunaj cos in sin
  if A(p,q) != 0
    
    tau = (A(p,p) - A(q,q)) / (2 * A(p,q));
    t = 1 / (abs(tau) + sqrt(1 + tau^2));
    if (tau < 0) t = -t; end; 
    c = 1/sqrt(1+t^2);
    s = c*t;
    
  end;


  % Mnozi po vrsticah
  tmpR1 = A(p,:); tmpR2 = A(q,:);
  A(p,:) =  c*tmpR1 + s*tmpR2;
  A(q,:) = -s*tmpR1 + c*tmpR2;


  % Mnozi po stolpcih
  tmpC1 = A(:,p); tmpC2 = A(:,q);
  A(:,p) =  c*tmpC1 + s*tmpC2;
  A(:,q) = -s*tmpC1 + c*tmpC2;
 
  % Še enkrat množim samo z desne - lastni vektorji
  tmpRR1 = R(:,p); tmpRR2 = R(:,q);
  R(:,p) =  c*tmpRR1 + s*tmpRR2;
  R(:,q) = -s*tmpRR1 + c*tmpRR2;
  
  numIter++;

  offDiagElems = norm(A - diag(diag(A)),'fro');

end;


% Komplikacije, ker ne dobim urejenih vektorjev...
Rh = R(:,end/2+1:end) * sqrt(2);
ds = diag(A)(end/2+1:end)';
H = zeros(size(R(:,1:end/2)));

[ds, Ind] = sort(ds);
ds = ds(end:-1:1);
Ind = Ind(end:-1:1);
Rh = Rh(:,Ind);

for i = 1:length(ds)
  j = 1;
      
  if dovoljBlizu(KA*Rh(:,i),ds(i)*Rh(:,i))
    break;
  end;
  
  Rh(:,i) *= -1;
  if dovoljBlizu(KA*Rh(:,i), ds(i)*Rh(:,i))
    break;
  end;
  
  
  Rh(1:end/2,i) *= -1;
  if dovoljBlizu(KA*Rh(:,i), ds(i)*Rh(:,i))
    break;
  end;
  
  
  Rh(1:end/2,i) *= -1;
  Rh(end/2+1:end,i) *= -1;
  if dovoljBlizu(KA*Rh(:,i), ds(i)*Rh(:,i))
    break;
  end;
  
end

V = Rh(1:end/2,:);
U = Rh(end/2+1:end,:);
S = diag(ds);



end

function r = dovoljBlizu(a, b, e = 1e-10)

r = (abs(a - b) < e);
r = sum(sum(r)) == prod(size(a));

end

function [p,q] = findAbsMaxTriu(A)

A = abs(A - diag(diag(A)));
[maxRow, maxRowIndex] = max(A);
[M, q] = max(maxRow);
p = maxRowIndex(q);

end
