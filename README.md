# Hitri SVD razcep

# Updates

- Napisal sem algoritem za Jacobija, ki deluje z matriko [0..0 A'; A 0..0] ...Hvala Peter :)

- razlaga: 
http://www-lp.fmf.uni-lj.si/plestenjak/vaje/NlaBol/Gradivo/05_EP_Jacobi.pdf
http://fourier.eng.hmc.edu/e176/lectures/ch1/node1.html
http://www.math.pitt.edu/~sussmanm/2071Spring08/lab09/index.html#Exercise2


- nahaja se v mapi Jacobi/Try3/jacobiAlgSym.m (Vrne U,S,V... ne deluje za nekvadratne)

- Najdu sm en boljši algoritem, ki sicer uporablja matriko A'*A, ampak je ne izračuna eksplicitno. Daje zelo natančne rezultate. To bo treba še dokumentirat, pa je...
http://www.netlib.org/lapack/lawnspdf/lawn15.pdf   stran 32... razlaga je še prej
http://www.math.pitt.edu/~sussmanm/2071Spring08/lab09/index.html tu je še matrial

- nahaja se v mapi Jacobi/Try2/jacobiAlg.m ... pošeneš jacobiAlg(A) in vrne U, S in V



## TODO:
- Miha: Dokumentiraj metodo, ki si jo napisal in testiraj na različno velikih matrikah. Lahko tudi testiraš na sliki (Kompresija) in prikašeš rezultate in narediš statistiko s časi in velikostjo... Primerjaj tudi z vgrajeno metodo
- Peter: Napiši uvod v nalogo in na kratko opiši SVD in zakaj se ga rabi in opiši, standardno metodo za SVD (tisto iz predavanj s pridobivanjem lastnih vrednosti in diagonalizacijo...) in napiši, zakaj ni dobra...jade jade...
- Lovro: ~~Jacobi do konca~~ in dokumentacija

- Če je kdo pri volji naj si pogleda Jacobijevo metodo (vsaj sproba naj)
- Predlagam LaTex Beamer, ker bo lažje enačbe not kucat, pa še ni tok zakompliciran za uporabljat...


## Ugotovitve

SIMETRIČNA matrika K = [zeros(size(A)) A'; A zeros(size(A))] ima lastne vrednosti, 
ki so enake singularnim vrednostim matrike A. K ima dvakrat več lastnih vrednosti
kot ima A singularnih, ker se lastne vrednosti dvakrat ponovijo z nasprotnim 
predznakom.



# Jacobijeva metoda...

Tu singularne vrednosti niso urejene po velikosti. 

Razloženo je tu:

- http://fourier.eng.hmc.edu/e176/lectures/ch1/node1.html
- http://www.southampton.ac.uk/~feeg6002/lecturenotes/feeg6002_numerical_methods08.html
- http://www-lp.fmf.uni-lj.si/plestenjak/vaje/NlaBol/Gradivo/05_EP_Jacobi.pdf
- http://www-lp.fmf.uni-lj.si/plestenjak/vaje/Primeri.htm &rarr; http://www-lp.fmf.uni-lj.si/plestenjak/vaje/nm/Gradivo/onesidejacobi.zip

V mapi Jacobi/TestJacobiWorkingCopied so datoteke iz http://www-lp.fmf.uni-lj.si/plestenjak/vaje/nm/Gradivo/onesidejacobi.zip predelane in komentirane.

Ni še po specifikacijah in lastnih vektorjev ne dobi pravilno. 

# Po posvetu z Damirjem

- Jacobijeva metoda res vrne neurejene singularne vrednosti
- Če jo napišemo do konca in bo vredu
- ~~Glede na to, da se ničle v matriki [0..0 AT; A 0..0] ne spreminjajo, lahko matriko predstavimo kot dve matriki (A, A') in diagonalni vektor in s tem prišparamo ful prostora. To bo velik bonus, he said...~~ 
- Izboljšava potenčne metode bi bila, da uporabimo namesto ATA, zgornjo matriko in uporabimo podobno foro... Tu je treba pazit, da odstranimo obe največji singularni vrednosti (prvo in tisto z obrnjenim predznakom).
- Damir priporoča 5 slajdov na predstavitvi... 
- Jest bi dal opis in kako animacijo, kako dela potenčna metoda (Kako iščemo vrednosti) in idejo Jacobijeve metode in pol mamo že pokrito v resnici


