function [U, E, V] = naivniSVDk2(A, k)
% A ... vhodna matrika
% k ... stevilo singularnih vrednosti
%
% E ... diagonalna matrika s k singularnimi vrednostmi
% U ... matrika s k lastnimi vektorji ATA
% V ... matrika s k lastnimi vektorji AAT

    % matrika katere lastne vrednosti so kvardrirane singularne
    % vrednosti matrike A
    B = [zeros(size(A)) A'; A zeros(size(A))];
    
    
    toleranca = eps;
    
    s = [];
    U = [];
    V = [];
    for i = 1:k
        % zacetni priblizek
        %u1 = stdnormal_rnd([rows(ATA),1]);
        u1 = sum(B, 2);
        u1 = u1 ./ norm(u1);
        u0 = ones(rows(B), 1) * 0;
        
        
        % iteriramo dokler ni razlika med iteracijama nižja od tolerance
        while norm(u0 - u1, "fro") > toleranca
            u0 = u1;
            u1 = (B * u0);
            u1 = u1 ./ norm(u1, "fro");
        endwhile
        
        
        V = [V u1(1:rows(A))];
        U = [U u1((rows(A)+1):end)];
        
        % lastna vrednost
        a = u1' * B * u1;
        keyboard()
        % iznicimo najdeno lastno vrednost
        B = B - a * u1 * u1';
        
        s = [s a];
        
    endfor
    
    E = diag(s);

end
