\documentclass[12pt,a4paper]{article}

\usepackage[slovene]{babel}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{listings}
\usepackage{inconsolata}
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{pdflscape}
\usepackage{bm}
\usepackage{subfig}
\usepackage{parskip}
\usepackage{hyperref}


\usepackage[total={7in,10in},a4paper]{geometry}

\usepackage{booktabs,array}
\def\Midrule{\midrule[\heavyrulewidth]}
\newcount\rowc



\newenvironment{changemargin}[2]{%
\begin{list}{}{%
\setlength{\topsep}{0pt}%
\setlength{\leftmargin}{#1}%
\setlength{\rightmargin}{#2}%
\setlength{\listparindent}{\parindent}%
\setlength{\itemindent}{\parindent}%
\setlength{\parsep}{\parskip}%
}%
\item[]}{\end{list}}


\graphicspath{slike}

\lstset{
    language=Octave, 
    basicstyle=\ttfamily\small,
    numberstyle=\footnotesize,
    numbers=left,
    %backgroundcolor=\color{gray!10},
    %frame=single,
    tabsize=2,
    %rulecolor=\color{black!30},
    %title=\lstname,
    escapeinside={\%*}{*)},
    breaklines=true,
    breakatwhitespace=true,
    framextopmargin=2pt,
    framexbottommargin=2pt,
    inputencoding=utf8,
    extendedchars=true,
    literate={č}{{\˘c}}1 {š}{{\˘s}}1 {ž}{{\˘z}}1 {Č}{{\˘c}}1 {Š}{{\˘S}}1 {Ž}{{\˘Z}}1,
}


\title{Odrezani SVD}
\date{}
\author{Lovro Suhadolnik, Mihael Verček, Peter Matičič}

\begin{document}

\maketitle

\section{Uvod in kaj je SVD}
 
SVD - singular value decomposition ali singularni razcep matrike vrednosti je faktorizacija neke poljubne matrike $A \in \mathbb{R}^{n*m}$ v obliko $U\Sigma V^T$, $U \in \mathbb{R}^{n*n}, \Sigma \in \mathbb{R}^{n*m}, V \in \mathbb{R}^{m*m}$. Elementi diagonalne matrike $\Sigma$ so singularne vrednosti matrike A, stolpci ortogonalne matrike U so levi lastni vektorji matrike A, in stolpci ortogonalne matrike V so desni lastni vektorji matrike A. 
\\

Tak razcep matrike je pogosto uporabljen v praksi: Moore-Penrosov inverz, izgubna kompresija slik, analiza podatkov, itd. Zaradi uporabnosti si želimo čim hitrejši razcep, tudi na račun natančnosti. Zato lahko zmanjšamo problem na ta način, da iščemo le $k$ največjih singularnih vrednosti matrike A in s tem tudi le toliko pripadajočih levih in desnih lastnih vektorjev. Pri tem ko smo izračunali le $k$ največjih singularnih vrednosti smo seveda izgubili nekaj natančnosti pri rekonstrukciji vendar v primeru izgubne kompresije je to tudi naš cilj. 
\\

Delo smo si razdelili tako, da je Lovro implementiral Jacobijevo metodo iskanja singularnih vrednosti in jo opisal, Mihael naivno metodo ter jo prav tako opisal, Peter pa pokazal povezavo med lastnimi vretnostmi nediagonalne bločne matrike $[0, A^T; A, 0]$ in singularnimi vrednostmi matrike $A$ ter sestavil in uredil končno poročilo.
 
\section{Računanje SVD}

Obstaja mnogo metod za računanje singularnega razcepa. Razvoj algoritmov za ta problem se dogaja še danes. Algoritmi, ki so v množični uporabi in so vgrajeni v matematične pakete in knjižnice (Matlab, LAPACK, BLAS) so precej hitri, a tudi precej zapleteni. Večina teh algoritmov sloni na QR razcepu matrike.
\\

Pri izdelavi naloge smo najprej začeli z naivno metodo iskanja singularnih vrednosti in singularnih vektorjev in nato poizkusili implementirati Jacobijevo metodo.
\\

Pri iskanju singularnih vrednosti matrike A lahko uporabimo klasičen pristop in poiščemo lastne vrednosti matrike $A^TA$ za katere velja da so ravno kvadrati singularnih vrednosti matrike A. Ta metoda ima težave pri zelo majhnih številih, saj se nam lahko zgodi da števila padejo izven območja dvojne natančnosti na današnjih računalnikih, zaradi česar postane računanje numerično nestabilno.
\\

V izogib numerični nestabilnosti uporabimo naslednjo matriko
\[
 B = \begin{bmatrix}
      0 & A^T\\ A & 0
     \end{bmatrix}
\]
katere lastnost je ta, da so njene lastne vrednosti pozitivne in negativne singularne vrednosti matrike A. Da to pokažemo, privzamemo, da že poznamo SVD razcep matrike $A$ in upoštevamo še nekatere druge spodaj naštete lastnosti, ki bodo pomagale pri dokazu.
\begin{gather*}
 A = U \Sigma V^T\\
 A^T = V \Sigma U^T\\
 AV = US\\
 A^TU = VS
\end{gather*}
Če vzamemo poljubno singularno vrednost matrike $A$ označeno z $s_i$, ki je posledično tudi pozitivna lastna vrednost matrike $A$, in njej pripadajoča singularna vektorja $u_i$ in $v_i$, ki skupaj sestavljata lastni vektor matrike $B$ (tu moramo upoštevati, da se njuna norma spremeni, zato ju sproti normiramo), lahko zapišemo naslednje:
\[
 \begin{bmatrix}
  0 & A^T\\A&0
 \end{bmatrix}
 \frac{1}{\sqrt{2}}
 \begin{bmatrix}
  \pm v_i\\u_i
 \end{bmatrix}
 =\frac{1}{\sqrt{2}}
 \begin{bmatrix}
  A^T\cdot u_i\\
  A\cdot \pm v_i
 \end{bmatrix}
 =\frac{1}{\sqrt{2}}
 \begin{bmatrix}
  v_is_i\\\pm u_is_i
 \end{bmatrix}
 =\pm\frac{1}{\sqrt{2}}s_i\begin{bmatrix}
      \pm v_i\\u_i
     \end{bmatrix}
\]
S tem smo pokazali da so lastni vektorji in pozitivne lastne vrednosti matrike $B$ dejansko singularni vektorji in singularne vrednosti matrike $A$ saj smo pokazali da drži pogoj za lastne vektorje in lastne vrednosti $Bv = \lambda v$. Tako smo dobili singularne vrednosti matrike $A$ in pripadajoče vektorje v dveh matrikah oblike

\[
 \begin{bmatrix}
  -S & 0\\
  0 & S
 \end{bmatrix}
 in
 \begin{bmatrix}
  V'' & V \\
  U'' & U
 \end{bmatrix}
\]


\section{Naivna metoda}

Naivna ali potenčna metoda, je iterativna metoda, ki poišče lastni vektor, ki
pripada največji lastni vrednosti diagonalizabilne matrike.
Iskali bomo lastne vrednosti matrike $A^TA$, ki ima lastne vrednosti kvadrate singularnih vrednsoti matrike $A$.
\\
Začnemo z naključnim normiranim vektorjem $v_0$. Iterativno računamo boljše približke lastnemu vektorju.
\[
  v_{k+1} = \frac{A^TA \cdot v_k}{\Arrowvert A^TA \cdot v_k \Arrowvert}
\]

To počnemo dokler ni norma med zaporednima iteracijama zanemarljiva.
Nato izračunamo pripadajočo lastno vrednsot.
\begin{align*}
 v^T \backslash A^TA v = \lambda v \\
 v^T A^TA v = \lambda
\end{align*}

Iz nje enostavno izračunamo singularno vrednost matrike A $s_i = \sqrt{\lambda_i}$.

Izračunano lastno vrednost moramo izničiti iz matrike, zato da lahko s ponovno iteracijo izračunamo drugo največjo lastno vrednost.
\[
  A^TA - \lambda v v^T
\]

Izračunali bomo $k$ največjih singularnih vrednsoti in pripadajoče leve in desne singularne vektorje.
\begin{lstlisting}
 for i = 1:k
\end{lstlisting}

Prvi približek levega in desnega singularnega vektorja.
\begin{lstlisting}
 u1 = sum(ATA, 2);
 u1 = u1 ./ norm(u1);
 u0 = ones(rows(ATA), 1) * 0;
 
 v1 = sum(AAT, 2);
 v1 = v1 ./ norm(v1);
 v0 = ones(columns(AAT), 1) * 0;
\end{lstlisting}

Nato iterativno pridelamo lastni vektor, ki odgovarja največji lastni vrednosti.

\begin{lstlisting}
while norm(u0 - u1, "fro") > toleranca
    u0 = u1;
    u1 = (ATA * u0);
    u1 = u1 ./ norm(u1, "fro");
endwhile

while norm(v0 - v1, "fro") > toleranca
    v0 = v1;
    v1 = (AAT * v0);
    v1 = v1 ./ norm(v1, "fro");
endwhile
\end{lstlisting}

Dobljene lastne vektorje shranimo in izračunamo pripadajočo lastno vrednost in singularno vrednost.

\begin{lstlisting}
V = [V v1];
U = [U u1];

% lastna vrednost
a = u1' * ATA * u1;

% singularna vrednost
s = [s; sqrt(a)];
\end{lstlisting}

Dobljeno lastno vrednost izničimo, da bomo lahko izračunali naslednjo največjo.

\begin{lstlisting}
ATA = ATA - a * u1 * u1';
AAT = AAT - b * v1 * v1';

endfor
\end{lstlisting}

\section{Jacobijeva Metoda}

Jacobijeva metoda je iterativna metoda, ki se uporablja za diagonalizacijo matrik in iskanje singularnih vrednosti in pripadajočih vektorjev. 
Deluje za simetrične matrike. Metoda deluje zaradi Givensovih rotacij. Matrika
\[
R = \begin{bmatrix}
cos\,\theta & -sin\,\theta \\
sin\,\theta & \quad cos\,\theta
\end{bmatrix} = \begin{bmatrix}
c & -s \\
s & \quad c
\end{bmatrix}
\]
je rotacijska matrika. Če vektor stolpec z leve pomnožimo s to matriko ga zasukamo za kot $\theta$ v pozitivno smer.
\[
\boldsymbol R x = \begin{bmatrix}
c & -s\\
s & \quad c
\end{bmatrix} \begin{bmatrix}
x_1 \\
x_2
\end{bmatrix} = \begin{bmatrix}
cx_1 - sx_2\\
sx_1 + cx_2
\end{bmatrix}
\]
Vrstico zasukamo tako, da jo pomnožimo z desne.
\[
x \boldsymbol{ R^T} = \begin{bmatrix}
x_1 & x_2 
\end{bmatrix} \begin{bmatrix}
c & s \\
-s & c
\end{bmatrix} = \begin{bmatrix}
cx_1-sx_2 & sx_1+cx_2
\end{bmatrix}
\]

Enak trik deluje tudi v treh dimenzijah. Obrate okrog koordinatnih osi dosežemo, če matrike množimo z leve in desne. To so rotacijske matrike: \[
\boldsymbol{R}_x = \begin{bmatrix}
1 & 0 & 0\\
0 & c & -s \\
0 & s & c
\end{bmatrix} \qquad \boldsymbol{R}_y = \begin{bmatrix}
c & 0 & s\\
0 & 1 & 0 \\
-s & 0 & c
\end{bmatrix} \qquad \boldsymbol{R}_z = \begin{bmatrix}
c & -s & 0\\
s & c  & 0 \\
0 & 0 & 1
\end{bmatrix}
\]

V večdimenzionalnem prostoru tudi lahko rotiramo in sicer okrog normale ravnine, ki jo napenjata vektorja stolpca $s_i$ in $s_j$. Rotacijska matrika je potem identiteta z dodatnimi elementi cos in sin \[
\boldsymbol R (i,j,\theta) = \begin{bmatrix}
1 & \hdots & 0 & \hdots & 0 & \hdots & 0\\
\vdots & \ddots & \vdots & &\vdots & &\vdots \\
0 & \hdots & c & \hdots & s & \hdots & 0\\
\vdots &  & \vdots & \ddots &\vdots & &\vdots \\
0 & \hdots & -s & \hdots & c & \hdots & 0\\
\vdots &  & \vdots &  &\vdots & \ddots &\vdots \\
0 & \hdots & 0 & \hdots & 0 & \hdots & 1\\
\end{bmatrix} \begin{tabular}{c c c}
R(i,i) & = & cos $\theta$ \\
R(j,j) & = & cos $\theta$ \\
R(i,j) & = & -sin $\theta$ \\
R(j,i) & = & sin $\theta$ \\
\end{tabular}
\]

Pri takem množenju se vedno spremenita samo stolpca $s_i$ in $s_j$ ter vrstici $v_i$ in $v_j$, ostali elementi ostanejo nespremenjeni.

Jakobijeva metoda poišče lastne vrednosti simetrične matrike tako, da postopno obrača vrstice in stolpce matrike, kjer pri vsakem koraku izniči en element izven diagonale. \[
\boldsymbol R = R(i,j,\theta) \qquad A' = \boldsymbol{R} A \boldsymbol R^T
\] Tako matrika postaja diagonalna. Lastne vektorje dobimo tako, da na začetku identično matriko z leve množimo z rotacijskimi matrikami, ki jih zgradimo na vsakem koraku postopka. \[
V_0 = I \qquad V = \boldsymbol{R}_n \boldsymbol{R}_{n-1} ... \boldsymbol{R}_2 \boldsymbol{R}_1 
\]
Ker je matrika A simetrična, je tudi produkt $\boldsymbol{R} A \boldsymbol{R}^T$ simetrična matrika.

Poglejmo si primer $A^{5\times 5}$ matrike, ki jo množimo z matriko $\boldsymbol{R}(2,4,\theta)$.

\[
R\,A\,R^T = \begin{bmatrix}
1 & 0 & 0 & 0 & 0 \\
0 & c & 0 & -s & 0\\
0 & 0 & 1 & 0 & 0\\
0 & s & 0 & c & 0 \\
0 & 0 & 0 & 0 & 1 
\end{bmatrix} \begin{bmatrix}
a_{11} & a_{12} & a_{13} & a_{14} & a_{15}\\
a_{21} & a_{22} & a_{23} & a_{24} & a_{25}\\
a_{31} & a_{32} & a_{33} & a_{34} & a_{35}\\
a_{41} & a_{42} & a_{43} & a_{44} & a_{45}\\
a_{51} & a_{52} & a_{53} & a_{54} & a_{55}\\
\end{bmatrix} \begin{bmatrix}
1 & 0 & 0 & 0 & 0 \\
0 & c & 0 & s & 0\\
0 & 0 & 1 & 0 & 0\\
0 & -s & 0 & c & 0 \\
0 & 0 & 0 & 0 & 1 
\end{bmatrix} = \]
\[
\begin{bmatrix}
a_{11} & ca_{12}-sa_{14} & a_{13} & sa_{12}+ca_{14} & a_{15}\\
ca_{21}-sa_{41} & c^2 a_{22}-cs(a_{24}+a_{42})+s^2 a_{43} & ca_{23}-sa_{43} & c^2 a_{24}+cs(a_{22}-a_{44})-s^2 a_{42} & ca_{25} - sa_{45}\\
a_{31} & ca_{32} - sa_{34} & a_{33} & sa_{32} + ca_{34} & a_{35}\\
sa_{21}+ca_{41} & c^2 a_{42}+cs(a_{22}-a_{44})-s^2 a_{24} & sa_{23}+ca_{43} & s^2 a_{22}+cs(a_{24}-a_{42})+c^2 a_{44} & sa_{25} + ca_{45}\\
a_{51} & ca_{52}-sa_{54} & a_{53} & sa_{52} + ca_{54} & a_{55}
\end{bmatrix}
\]

Če želimo izničiti element $a_{ij}$, mora veljati naslednja zveza: \[
(c^2 - s^2)a_{ij} + cs(a_{ii} - a_{jj}) = 0 \]
\[(c^2 - s^2)a_{ij} = -cs(a_{ii} - a_{jj}) \]
\[ \frac{c^2 - s^2}{cs} \frac{\frac{1}{c^2}}{\frac{1}{c^2}} = \frac{1 - \frac{s^2}{c^2}}{\frac{s}{c}} = \frac{1 - t^2}{t} = \frac{a_{jj} - a_{ii}}{a_{ij}} = 2\omega \]
\[\frac{1-t^2}{2t} = \omega = \frac{a_{jj} - a_{ii}}{2a_{ij}}\]
\[0 = t^2 + 2\omega - 1 \]

Rešitev te enačbe je \[
t_{1,2} = -\omega \pm \sqrt{\omega^2 + 1}
\]. Vzamemo rešitev, ki ima najmanjšo absolutno vrednost. \[
t = \frac{signum(\omega)}{|\omega| + \sqrt{1+\omega^2}}  \qquad signum(x) = \begin{cases}
1 & x \geq 0 \\
-1 & x < 0
\end{cases}
\].
Velja še \[
cos^2 (a) =  \frac{cos^2 (a)}{1} = \frac{cos^2 (a) /:cos^2 (a)}{sin^2 (a) + cos^2 (a) /:cos^2 (a)} = \frac{1}{tan^2 (a) + 1}
\]\[
cos (a) = \frac{1}{\sqrt{tan^2 (a) + 1}}
\]
\[sin(a) = tan (a) * cos(a)\]

Sedaj znamo izračunati ti števili in lahko opišemo celoten postopek:
\begin{enumerate}
\item{Izberemo element, ki ga želimo uničiti}
\item{Izračunamo števili s = sin$\theta$ in c = cos$\theta$}
\item{Zmnožimo ustrezne vrstice in stolpce matrike}
\item{Zmnožimo še vrstice matrike R (za lastne vektorje)}
\end{enumerate}

\subsection{Klasična metoda}

Na spletu je mogoče najti več primerov izvajanja Jacobijeve iteracije. Lahko se odločimo, da na vsakem koraku uničimo največji element izven diagonale po absolutni vrednosti. To je t.i. klasična metoda. Ker Jacobijeva metoda deluje le za simetrične matrike, smo uporabili trik in originalno matriko A razširili v matriko $\begin{bmatrix}
0 & A^T\\
A & 0
\end{bmatrix}$. Izkaže se, da so lastne vrednosti te matrike enake singularnim vrednostim matrike A, matrika lastnih vektorjev pa je enaka $\frac{1}{\sqrt{2}} \begin{bmatrix}
U^* & U \\
V^* & V \\
\end{bmatrix}$, kjer matrika V predstavlja matriko desno singularnih vektorjev in U matriko levo singularnih vrednosti.

Sedaj lahko začnemo sestavljati algoritem. Najprej razširimo matriko z ničlami
\begin{lstlisting}
za = zeros(size(A));
A = [za A'; A za];
\end{lstlisting}
Sledi glavna zanka algoritma, ki se izvaja, dokler vsota elementov izven diagonale ne doseže željene spodnje meje.
\begin{lstlisting}
while offDiagElems > 1e-7
\end{lstlisting}
Potem sledim postopku in poiščem položaj največjega elementa v matriki
\begin{lstlisting}    
  [p,q] = findAbsMaxTriu(A);
\end{lstlisting}

Nato izračunam števili s in c. Če je element na položaju A(p,q) enak 0, potem ne računam ampak vzamem konstanti c = 1 in s = 0;
\begin{lstlisting}
  c = 1; s = 0;
  if A(p,q) != 0    
    tau = (A(p,p) - A(q,q)) / (2 * A(p,q));
    t = 1 / (abs(tau) + sqrt(1 + tau^2));
    if (tau < 0) t = -t; end; 
    c = 1/sqrt(1+t^2);
    s = c*t;    
  end;
\end{lstlisting}
Ko ju imam, lahko posodobim vrstice in stolpce - tu uničim element na položaju (p,q)
\begin{lstlisting}
  % Mnozi po vrsticah
  tmpR1 = A(p,:); tmpR2 = A(q,:);
  A(p,:) =  c*tmpR1 + s*tmpR2;
  A(q,:) = -s*tmpR1 + c*tmpR2;

  % Mnozi po stolpcih
  tmpC1 = A(:,p); tmpC2 = A(:,q);
  A(:,p) =  c*tmpC1 + s*tmpC2;
  A(:,q) = -s*tmpC1 + c*tmpC2;
\end{lstlisting}
Nato posodobim še matriko R, ki bo na koncu vsebovala lastne vektorje
\begin{lstlisting}
  % Se enkrat mnozim samo z desne - lastni vektorji
  tmpRR1 = R(:,p); tmpRR2 = R(:,q);
  R(:,p) =  c*tmpRR1 + s*tmpRR2;
  R(:,q) = -s*tmpRR1 + c*tmpRR2;
\end{lstlisting}

Ko smo pisali program, smo ga testirali na manjših matrikah do velikosti 10 in algoritem je dajal dobre rezultate, ko pa povečujemo velikost matrike, pa rezultati postajajo čedalje slabši. Algoritem ne vrača pravilnih vrednosti, tudi če zmanjšujemo pogoj za konvergenco. Drug problem je, da lastne vrednsoti računa naurejeno, zato odpade možnost računanja k največjih. Tretji problem pa je, da imajo nekateri stolpci matrike lastnih vektorjev obrnjene predznake in jih je treba preobračati.

Test: Povprečni čas 10 ponovitev na naključni matriki celih števil. Na večjih primerih algoritem ni dal pravih rezultatov, zato so meritve izključene.\\ \hfill \\
\begin{tabular}{ c | c | c | c | c | c | c | c | c | c | c |}
Velikost matrike & 2x2 & 3x3 & 4x4 & 5x5 & 6x6 & 7x7 & 8x8 & 9x9 & 10x10 & 11x11 \\ 
Povprečen čas & 0.008 & 0.015 & 0.022 & 0.036 & 0.067 & 0.080 & 0.108 & 0.147 & 0.174 & 0.216  \\
\end{tabular}\\ \hfill \\
Precej veliko časa vzame pregledovanje lastnih vektorjev.

\subsection{Predelava - enostranska metoda}

Na spletu smo našli še en algoritem, ki daje boljše rezultate. Množi le z desne - spreminjajo se le stolpci (zato je enostranska metoda). Ne išče absolutno največjega elementa, ampak se iterativno sprehaja čez stolpce. Pri tem implicitno računam matriko $A^TA$.

Prejšnji algoritem je vrednost $\omega$ izračunal na podlagi vrednosti, ta pa jo izračuna na podlagi celih stolpcev.
\begin{lstlisting}
  prviStolpec = U(:,i)'*U(:,i);
  drugiStolpec = U(:,j)'*U(:,j);
  zmnozekStolpcev = U(:,i)'*U(:,j);
\end{lstlisting}

Izračuna vrednosti s in c.
\begin{lstlisting}
  theta = (drugiStolpec - prviStolpec)/(2*zmnozekStolpcev);
  t = sign(theta)/(abs(theta) + sqrt(1+theta^2));

  c = 1/sqrt(1+t^2);
  s = c*t;
\end{lstlisting}

in posodobi vrednosti v matrikah
\begin{lstlisting}
  % Posodobi stolpce v U - mnozimo samo z desne
	tmp = U(:,i);
  U(:,i)=c*tmp-s*U(:,j);
  U(:,j)=s*tmp+c*U(:,j);
  
  % Posodobi stolpce v V - mnozimo samo z desne
  tmp = V(:,i);
  V(:,i) = c*tmp - s*V(:,j);
  V(:,j) = s*tmp + c*V(:,j);
\end{lstlisting} 

Singularne vrednosti bodo norme stolpcev v U. Levi singularni vektorji bodo normirani stolpci matrike U.
\begin{lstlisting}
for j=1:n
  S(j)=norm(U(:,j));
  U(:,j)=U(:,j)/S(j);
end
\end{lstlisting}

\section{Primerjava implementiranih algoritmov}

Izboljšani Jacobijev algoritem je tudi malo hitrejši od originalnega, a se vseeno ne more kosati z vgrajenim.
Test: Povprečni čas 10 ponovitev na naključni matriki celih števil.\\ \hfill \\
\begin{tabular}{c | c | c|c|c|c|c|c|c|c|c}
size(A) & 10x10  & 20x20 & 30x30 & 40x40 & 50x50 & 60x60 & 70x70 & 80x80 & 90x90 & 100x100 \\
$t_{Alg}$ &  0.06 &  0.22 &  0.57 &  1.17 &  1.86 &  3.04 &  4.17 &  5.58 &  7.17 &  9.29  \\
$t_{svd}$ & 0.003 &  0.01 &  0.02 &  0.02 &  0.03 &  0.04 &  0.04 &  0.05 &  0.06 &  0.11  \\
\end{tabular}

\smallskip

Na slkah ~\ref{fig:kompersiranaLena} lahko opazimo, da naivna metoda proizvede sliko mnogo slabše kvalitete. Napake v kompersiji izvirajo
iz približka matrike $A$ na kateri smo računali lastne vrednosti.

\begin{figure}%
  \centering
  \subfloat[Vgrajena metoda]{{\includegraphics[width=6cm]{../../Naivni/svds} }}%
  \qquad
  \subfloat[Naivna metoda]{{\includegraphics[width=6cm]{../../Naivni/naivni} }}%
  \caption{Kompersija Lene z dvema metodama}%
  \label{fig:kompersiranaLena}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=12cm]{../../Naivni/timed}
\caption{Čas potreben za izračun $x$ singularnih vrednosti na sliki Lene} 
\label{fig:primerjavaCasov}
\end{figure}

Časovna primerjava ~\ref{fig:primerjavaCasov} nam pokaže, da se potenčna metoda zares obnaša potenčno, medtem ko je kompleksnost Jacobijeva iteracije na prvi pogled
kosntantna. Najhitrejša izvedba je vgrajena metoda, označena z rdečo. Za test smo uporabili računanje SVD razcepa na sliki Lene za različno število singularnih vrednosti (1 - 50).


\section{Zaključek}
Kot se izkaže nam je uspelo izboljšati časovno zahtevnost algoritma, vsaj pri prehodu iz naivnega na obe Jacobijevi metodi in za nekaj malega tudi med obema Jacobijevima, vendar so ostali algoritmi, ki so vgrajeni v samem programu Octave že precej bolj optimizirani in hitrejši kot naši.

\section{Viri}
\begin{itemize}
 \item \url{http://fourier.eng.hmc.edu/e176/lectures/ch1/node1.html} (8. 6. 2018)
 \item \url{https://www.fmf.uni-lj.si/~plestenjak/Vaje/NlaBol/Gradivo/05_EP_Jacobi.pdf} (8. 6. 2018)
 \item \url{https://www.fmf.uni-lj.si/~plestenjak/Vaje/NlaBol/Gradivo/12_SVD_Racunanje.pdf} (8. 6. 2018)
 \item \url{http://www.math.pitt.edu/~sussmanm/2071Spring08/lab09/index.html} (8. 6. 2018)
 \item \url{http://www.netlib.org/lapack/lawnspdf/lawn15.pdf} (8. 6. 2018)
\end{itemize}


\end{document}
