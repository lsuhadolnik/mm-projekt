function [U, E, V] = naivniSVDk(A, k)
% A ... vhodna matrika
% k ... stevilo singularnih vrednosti
%
% E ... diagonalna matrika s k singularnimi vrednostmi
% U ... matrika s k lastnimi vektorji ATA
% V ... matrika s k lastnimi vektorji AAT

    % matrika katere lastne vrednosti so kvardrirane singularne
    % vrednosti matrike A
    ATA = [];
    AAT = [];
    if rows(A) >= columns(A)
        ATA = A' * A;
        AAT = A * A';
    else
        ATA = A * A';
        AAT = A' * A;
    endif
    
    
    toleranca = eps;
    
    s = [];
    U = [];
    V = [];
    for i = 1:k
        % zacetni priblizek
        %u1 = stdnormal_rnd([rows(ATA),1]);
        u1 = sum(ATA, 2);
        u1 = u1 ./ norm(u1);
        u0 = ones(rows(ATA), 1) * 0;
        
        % zacetni priblizek
        %v1 = stdnormal_rnd([columns(AAT),1]);
        v1 = sum(AAT, 2);
        v1 = v1 ./ norm(v1);
        v0 = ones(columns(AAT), 1) * 0;
        
        % iteriramo dokler ni razlika med iteracijama nižja od tolerance
        while norm(u0 - u1, "fro") > toleranca
            u0 = u1;
            u1 = (ATA * u0);
            u1 = u1 ./ norm(u1, "fro");
        endwhile
        
         % iteriramo dokler ni razlika med iteracijama nižja od tolerance
        while norm(v0 - v1, "fro") > toleranca
            v0 = v1;
            v1 = (AAT * v0);
            v1 = v1 ./ norm(v1, "fro");
        endwhile
        
        V = [V v1];
        
        % lastna vrednost
        a = u1' * ATA * u1;
        b = v1' * AAT * v1;
        U = [U u1];
        
        % iznicimo najdeno lastno vrednost
        ATA = ATA - a * u1 * u1';
        AAT = AAT - b * v1 * v1';
        s = [s; sqrt(a)];
        
    endfor
    
    E = diag(s);

end
