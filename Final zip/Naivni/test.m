addpath("../Jacobi/Try3/");

A = imread("lena512.mat");
A = double(A);

k = 10;
[U, E, V] = svds(A, k);
compr = U * E * V';

%figure(1)
%imshow(compr, [0, 255]);


[U1, E1, V1] = naivniSVDk(A, k);
compr1 = (-V1) * E1 * (-U1)';
%figure(2)
%imshow(compr1, [0, 255]);

[U2, E2, V2] = naivniSVDk2(A, k);

keyboard()

%figure(3)
%[U2, E2, V2, n] = jacobiAlgSym(A);
%compr2 = V2(:,1:k) * E2(1:k,1:k) * U2(:,1:k)';
%imshow(compr2, [0, 255]);
