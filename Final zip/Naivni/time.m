addpath("../Jacobi/Try3/");

A = imread("lena512.mat");
A = double(A);

casi_svds = [];
casi_naivni = [];
casi_jacobi = [];
for i = 1:50
    tic();
    [U, E, V] = svds(A, i);
    casi_svds = [casi_svds toc()];
    
    tic();
    [U, E, V] = naivniSVDk(A, i);
    casi_naivni = [casi_naivni toc()];
    
    tic();
    [U, E, V] = jacobiAlgSym(A);
    casi_jacobi = [casi_jacobi toc()];
endfor

plot(casi_svds, "r");
hold on
plot(casi_naivni, "b");
hold on
plot(casi_jacobi, "g");
legend({"Vgrajena metoda", "Naivna metoda", "Jacobijeva metoda"}, "location", "northwest");
legend boxoff

xlabel("Odrezani SVD razcep za x singularnih vrednosti");
ylabel("Čas v sekundah");
print timed2.jpg
