function [casi, stIteracij, rezultat] = testAlg()

n = 10;
casi = [];
stIteracij = [];

        for i = 2:30


          
          psti = 0;
          ptime = 0;

          for j = 1:n
                    
                    A = randi(100, i, i);

                    tic;
                    [U,S,V, sti] = jacobiAlgSym(A);
                    time = toc;
                    psti += sti;
                    ptime += time;
            
          end;
          
          casi = [casi ptime/n];
          stIteracij = [stIteracij psti/n];

        end

end
