function [U,S,V, stIteracij] = jacobiAlg2(A)


toleranca = 1e-6;


% Pogoj za konvergenco 
pogoj = 100;
stIteracij = 0;


% Dolžina stranice, predpostavka, da je matrika kvadratna
n = length(A);

% Na začetku je U enak A, na koncu bodo tu notri levo-singularni vektorji
U = A;
% Na koncu bodo tu desno-singularni vektorji
V = eye(n);

% Dokler stvar ne konvergira
while pogoj > toleranca
pogoj = 0;

for j = 2:n
  for i = 1:j-1
  
	% ...Seštej in kvadriraj prvi stolpec
  prviStolpec = U(:,i)'*U(:,i);
  % ...Seštej in kvadriraj drugi stolpec
  drugiStolpec = U(:,j)'*U(:,j);
  % ...Zmnoži ta dva stolpca
  zmnozekStolpcev = U(:,i)'*U(:,j);

	pogoj = max(pogoj, abs(zmnozekStolpcev)/sqrt(prviStolpec*drugiStolpec));

  % Izračunam s in c
  theta = (drugiStolpec - prviStolpec)/(2*zmnozekStolpcev);
  t = sign(theta)/(abs(theta) + sqrt(1+theta^2));

  c = 1/sqrt(1+t^2);
  s = c*t;

  % Posodobi stolpce v U - množimo samo z desne
	tmp = U(:,i);
  U(:,i)=c*tmp-s*U(:,j);
  U(:,j)=s*tmp+c*U(:,j);
  
  % Poskrbimo za lastne vektorje
  % Posodobi stolpce v V - množimo samo z desne
  tmp = V(:,i);
  V(:,i) = c*tmp - s*V(:,j);
  V(:,j) = s*tmp + c*V(:,j);


  end; % ENDFOR I
end; % ENDFOR J

  stIteracij++;

end; % END Jacobi


% Singularne vrednosti bodo norme stolpcev v U
% Levi singularni vektorji bodo 
for j=1:n

  S(j)=norm(U(:,j));
  U(:,j)=U(:,j)/S(j);

end % ENDFOR - poračuna levo singularne vektorje in singularne vrednosti
% Zberem singularne vrednosti v matriki S
S=diag(S);

end % ENDFUNCTION
